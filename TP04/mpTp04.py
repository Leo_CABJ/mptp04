
import os  

def tecla(s):
    print('')
    if s != 6:
       input('Presione una tecla .....')
       os.system('cls')

def Menu():
    print('------------------MENU--------------------')
    print(' 1)_ CARGAR PRODUCTOS, ')
    print(' 2)_ MOSTRAR EL LISTADO DE PRODUCTOS')
    print(' 3)_ MOSTRAR PRODUCTOS EN UN INTERVALO')
    print(' 4)_ SUMAR CANTIDAD A STOCK')
    print(' 5)_ ELIMINAR PRODUCTOS CUYO STOCK SEA 0')
    print(' 6)_ SALIR')
    print('-------------------------------------------')

    resp = int(input('Elija una opción:   '))

    while not(resp >= 1 and resp <= 6):
        print('La opcion ingresada es invalida')
        resp = int(input('Elija una opción:   '))
    os.system('cls')    
    return resp

def mostrar(diccionario):
    print('Lista de Productos')
    for codigo, valor in diccionario.items():
        print(codigo,valor)
    return diccionario

def leerCod():
    cod = input('Ingrese un codigo(por lo menos 4 caracteres):  ')
    while len(cod) < 4: #Un codigo razonable
        cod = input('Ingrese codigo:  ')    
    return cod

def leerDescripcion():
    descripcion = input('Ingrese una descripcion(por lo menos 5 caracteres):  ')
    while len(descripcion) < 5: #Un descripcion de al menos una palabra
        descripcion = input('Ingrese descripción:  ')    
    return descripcion  

def leerStock():
    stock = int(input('Ingrese stock:  '))
    while stock < 0: 
        print('Stock ingresado invalido')  
        stock = int(input('Ingrese stock:   '))  
    return stock  

def leerPrecio():
    precio = float(input('Ingrese el precio:   '))
    while precio <= 0: 
        print('Precio ingresado invalido')  
        precio = float(input('Ingrese stock:   '))  
    return precio         

def leerProductos(dic):
    print('Cargar lista de Productos')
    cod = 0
    resp = 's'
    while resp == 's' or resp == 'S':
        cod = leerCod()
    
        if cod not in dic:
           descripcion = leerDescripcion()
           stock = leerStock()
           precio = leerPrecio()
           dic[cod] = [descripcion, precio,stock]
        else:
                print('El codigo ya existe') 

        resp = input('Desea continuar?  s/n...   ') 

    return dic     

def leerIntervalo():
    a = int(input('Desde:  '))
    while a <0:
        a = int(input('Desde:  '))
    b = int(input('Hasta:   '))
    while b <= a:
        print('Valor invalido (debe ser mayor que el anterior numero ingresado)')
        b = int(input('Hasta:   '))
    return a,b

def mostrarIntervalo(diccionario):
    print('Ingresar intervalo de stock a mostrar [  ,  ]:   ')
    desde,hasta = leerIntervalo()

    for cod,lista in diccionario.items():
        if (lista[2] >= desde and lista[2] <= hasta):
            print(cod,lista)

def Cantidad(diccionario):
    print('Sumar una cantidad a los stocks')
    print('')
    cont =0
    cantidad = leerCantidad()
    print('A partir de que valor desea sumar al stock')
    stock = leerStock()
    for cod,lista in diccionario.items():
        if lista[2] < stock:  
           lista[2] += cantidad
           cont += 1
           if cont == 1:  #Si cargo por lo menos 1 vez me muestre el mensaje
               print('Sumado correctamente....')
            
    return diccionario         


def leerCantidad():
    cantidad = int(input('Ingrese la cantidad a sumar:  '))
    while cantidad < 0: 
        print('Cantidad ingresada invalida')  
        cantidad = int(input('Ingrese la cantidad a sumar:   '))  
    return cantidad      

def  eliminar(diccionario):
    codElim = []
    
    for cod,lista in diccionario.items():
        if lista[2] == 0:
            codElim.append(cod) 
    for i in range(len(codElim)):
        del diccionario[codElim[i]]

    print('Se eliminaron correctamente ',len(codElim), 'elementos')
    
    return diccionario

productos = {}

opc =99
os.system('cls')
productos = {}
while opc != 6:    
    opc = Menu()
    if opc == 1:
        productos = leerProductos(productos)
    elif opc == 2:
        mostrar(productos)
    elif opc == 3:
        mostrarIntervalo(productos)   
    elif opc == 4:
        Cantidad(productos)  
    elif opc == 5:
        eliminar(productos)
    elif opc == 6:
        print('Hasta pronto GRACIAS')

    tecla(opc)    
